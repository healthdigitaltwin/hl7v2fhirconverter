# HL7 to FHIR Converter API

This project provides an API for converting HL7 messages to FHIR format. It is implemented using the Quarkus framework.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Java Development Kit (JDK) version 11 or newer
- Gradle Build Tool

### Installing

1. Clone the repository to your local machine.
    ```
    git clone https://gitlab.com/healthdigitaltwin/hl7v2fhirconverter.git
    ```
2. Navigate into the project directory.
    ```
    cd hl7v2fhirconverter
    ```
3. Build the project using Gradle.
    ```
    ./gradlew build
    ```

## Running the Application

To run the application, execute the following command:

```
./gradlew quarkusDev
```


This will start the application in dev mode.

## Running the Tests

To run the tests, execute the following command:


```

./gradlew test

```


## API Usage

The API provides a POST endpoint at `/convert` which accepts HL7 messages in plain text and returns a FHIR format message.

## Contributing

Please read our [contributing guide](https://gitlab.com/healthdigitaltwin/assets/-/blob/main/CONTRIBUTING.md) for details on our code of conduct and the process for submitting merge requests to us.

## Code of Conduct

Please read our [Code of Conduct](https://gitlab.com/healthdigitaltwin/assets/-/blob/main/CODE_OF_CONDUCT.md) to understand the expectations for participating in our community.

## License

This project is licensed under the Apache License 2.0 - see the `LICENSE.md` file for details.
