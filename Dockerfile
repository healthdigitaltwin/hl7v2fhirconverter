# Build stage
FROM adoptopenjdk:11-jdk-hotspot as build
WORKDIR /app
COPY . /app
RUN ./gradlew build

# Run stage
FROM adoptopenjdk:11-jdk-hotspot
WORKDIR /app
COPY --from=build /app/RestAPI/build/libs/RestAPI-1.0.0.jar RestAPI.jar
ENTRYPOINT ["java", "-jar", "RestAPI.jar"]
CMD ["-Djava.util.logging.manager=org.jboss.logmanager.LogManager"]
EXPOSE 8080
EXPOSE 8443