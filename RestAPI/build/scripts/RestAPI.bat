@rem
@rem Copyright 2015 the original author or authors.
@rem
@rem Licensed under the Apache License, Version 2.0 (the "License");
@rem you may not use this file except in compliance with the License.
@rem You may obtain a copy of the License at
@rem
@rem      https://www.apache.org/licenses/LICENSE-2.0
@rem
@rem Unless required by applicable law or agreed to in writing, software
@rem distributed under the License is distributed on an "AS IS" BASIS,
@rem WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@rem See the License for the specific language governing permissions and
@rem limitations under the License.
@rem

@if "%DEBUG%"=="" @echo off
@rem ##########################################################################
@rem
@rem  RestAPI startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%"=="" set DIRNAME=.
@rem This is normally unused
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Resolve any "." and ".." in APP_HOME to make it shorter.
for %%i in ("%APP_HOME%") do set APP_HOME=%%~fi

@rem Add default JVM options here. You can also use JAVA_OPTS and REST_API_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if %ERRORLEVEL% equ 0 goto execute

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto execute

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\RestAPI-1.0.0.jar;%APP_HOME%\lib\HL7v2Converter-1.0.1-SNAPSHOT.jar;%APP_HOME%\lib\swagger-jaxrs2-2.2.14.jar;%APP_HOME%\lib\quarkus-resteasy-reactive-jackson-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-resteasy-reactive-jackson-common-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-jackson-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-resteasy-reactive-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-resteasy-reactive-common-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-vertx-http-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-vertx-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-mutiny-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-smallrye-context-propagation-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-netty-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-arc-3.2.0.Final.jar;%APP_HOME%\lib\commons-math3-3.6.1.jar;%APP_HOME%\lib\jackson-datatype-jdk8-2.15.2.jar;%APP_HOME%\lib\swagger-integration-2.2.14.jar;%APP_HOME%\lib\swagger-core-2.2.14.jar;%APP_HOME%\lib\jackson-dataformat-yaml-2.15.2.jar;%APP_HOME%\lib\jackson-jaxrs-json-provider-2.15.2.jar;%APP_HOME%\lib\swagger-models-2.2.14.jar;%APP_HOME%\lib\jackson-module-jaxb-annotations-2.15.2.jar;%APP_HOME%\lib\jackson-annotations-2.15.2.jar;%APP_HOME%\lib\jackson-jaxrs-base-2.15.2.jar;%APP_HOME%\lib\jackson-module-parameter-names-2.15.2.jar;%APP_HOME%\lib\resteasy-reactive-vertx-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-vertx-http-dev-console-runtime-spi-3.2.0.Final.jar;%APP_HOME%\lib\smallrye-mutiny-vertx-web-3.5.0.jar;%APP_HOME%\lib\vertx-web-4.4.4.jar;%APP_HOME%\lib\smallrye-mutiny-vertx-web-common-3.5.0.jar;%APP_HOME%\lib\smallrye-mutiny-vertx-auth-common-3.5.0.jar;%APP_HOME%\lib\smallrye-mutiny-vertx-bridge-common-3.5.0.jar;%APP_HOME%\lib\smallrye-mutiny-vertx-uri-template-3.5.0.jar;%APP_HOME%\lib\smallrye-mutiny-vertx-core-3.5.0.jar;%APP_HOME%\lib\smallrye-common-vertx-context-2.1.0.jar;%APP_HOME%\lib\vertx-web-common-4.4.4.jar;%APP_HOME%\lib\vertx-auth-common-4.4.4.jar;%APP_HOME%\lib\vertx-bridge-common-4.4.4.jar;%APP_HOME%\lib\vertx-mutiny-generator-3.5.0.jar;%APP_HOME%\lib\smallrye-mutiny-vertx-runtime-3.5.0.jar;%APP_HOME%\lib\vertx-uri-template-4.4.4.jar;%APP_HOME%\lib\vertx-core-4.4.4.jar;%APP_HOME%\lib\vertx-codegen-4.4.4.jar;%APP_HOME%\lib\jackson-core-2.15.2.jar;%APP_HOME%\lib\hapi-fhir-structures-r4-6.6.1.jar;%APP_HOME%\lib\hapi-fhir-validation-6.6.1.jar;%APP_HOME%\lib\hapi-fhir-structures-r5-6.6.1.jar;%APP_HOME%\lib\hapi-fhir-validation-resources-r4-6.6.1.jar;%APP_HOME%\lib\org.hl7.fhir.validation-6.0.1.jar;%APP_HOME%\lib\hapi-fhir-converter-6.6.1.jar;%APP_HOME%\lib\org.hl7.fhir.convertors-6.0.1.jar;%APP_HOME%\lib\org.hl7.fhir.r4-6.0.1.jar;%APP_HOME%\lib\hapi-fhir-caching-caffeine-6.6.1.jar;%APP_HOME%\lib\hapi-fhir-caching-api-6.6.1.jar;%APP_HOME%\lib\org.hl7.fhir.r5-6.0.1.jar;%APP_HOME%\lib\org.hl7.fhir.dstu2-6.0.1.jar;%APP_HOME%\lib\org.hl7.fhir.dstu2016may-6.0.1.jar;%APP_HOME%\lib\org.hl7.fhir.dstu3-6.0.1.jar;%APP_HOME%\lib\org.hl7.fhir.r4b-6.0.1.jar;%APP_HOME%\lib\org.hl7.fhir.utilities-6.0.1.jar;%APP_HOME%\lib\hapi-fhir-base-6.6.1.jar;%APP_HOME%\lib\jackson-datatype-jsr310-2.15.2.jar;%APP_HOME%\lib\resteasy-reactive-jackson-3.2.0.Final.jar;%APP_HOME%\lib\jackson-databind-2.15.2.jar;%APP_HOME%\lib\guava-32.0.0-jre.jar;%APP_HOME%\lib\commons-io-2.13.0.jar;%APP_HOME%\lib\hapi-structures-v26-2.3.jar;%APP_HOME%\lib\hapi-base-2.3.jar;%APP_HOME%\lib\commons-configuration2-2.9.0.jar;%APP_HOME%\lib\commons-text-1.10.0.jar;%APP_HOME%\lib\commons-jexl3-3.2.jar;%APP_HOME%\lib\json-path-2.8.0.jar;%APP_HOME%\lib\commons-collections4-4.4.jar;%APP_HOME%\lib\commons-compress-1.23.0.jar;%APP_HOME%\lib\fhir-term-4.11.1.jar;%APP_HOME%\lib\fhir-registry-4.11.1.jar;%APP_HOME%\lib\commons-beanutils-1.9.4.jar;%APP_HOME%\lib\snakeyaml-2.0.jar;%APP_HOME%\lib\brotli4j-1.12.0.jar;%APP_HOME%\lib\fhir-cache-4.11.1.jar;%APP_HOME%\lib\caffeine-3.1.5.jar;%APP_HOME%\lib\jsr305-3.0.2.jar;%APP_HOME%\lib\gson-2.10.1.jar;%APP_HOME%\lib\error_prone_annotations-2.19.1.jar;%APP_HOME%\lib\failureaccess-1.0.1.jar;%APP_HOME%\lib\j2objc-annotations-2.8.jar;%APP_HOME%\lib\httpclient-4.5.14.jar;%APP_HOME%\lib\commons-codec-1.16.0.jar;%APP_HOME%\lib\quarkus-jsonp-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-security-runtime-spi-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-core-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-bootstrap-runner-3.2.0.Final.jar;%APP_HOME%\lib\org-crac-0.1.3.jar;%APP_HOME%\lib\netty-codec-haproxy-4.1.94.Final.jar;%APP_HOME%\lib\netty-codec-http2-4.1.94.Final.jar;%APP_HOME%\lib\netty-handler-proxy-4.1.94.Final.jar;%APP_HOME%\lib\netty-codec-http-4.1.94.Final.jar;%APP_HOME%\lib\netty-resolver-dns-4.1.94.Final.jar;%APP_HOME%\lib\netty-handler-4.1.94.Final.jar;%APP_HOME%\lib\netty-codec-socks-4.1.94.Final.jar;%APP_HOME%\lib\netty-codec-dns-4.1.94.Final.jar;%APP_HOME%\lib\netty-codec-4.1.94.Final.jar;%APP_HOME%\lib\netty-transport-native-unix-common-4.1.94.Final.jar;%APP_HOME%\lib\netty-transport-4.1.94.Final.jar;%APP_HOME%\lib\netty-buffer-4.1.94.Final.jar;%APP_HOME%\lib\netty-resolver-4.1.94.Final.jar;%APP_HOME%\lib\netty-common-4.1.94.Final.jar;%APP_HOME%\lib\quarkus-credentials-3.2.0.Final.jar;%APP_HOME%\lib\arc-3.2.0.Final.jar;%APP_HOME%\lib\resteasy-reactive-3.2.0.Final.jar;%APP_HOME%\lib\resteasy-reactive-common-3.2.0.Final.jar;%APP_HOME%\lib\resteasy-reactive-common-types-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-security-2.0.2.Final.jar;%APP_HOME%\lib\quarkus-development-mode-spi-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-fs-util-0.0.9.jar;%APP_HOME%\lib\quarkus-ide-launcher-3.2.0.Final.jar;%APP_HOME%\lib\quarkus-vertx-latebound-mdc-provider-3.2.0.Final.jar;%APP_HOME%\lib\mutiny-smallrye-context-propagation-2.3.1.jar;%APP_HOME%\lib\mutiny-2.3.1.jar;%APP_HOME%\lib\smallrye-config-3.3.0.jar;%APP_HOME%\lib\smallrye-config-core-3.3.0.jar;%APP_HOME%\lib\smallrye-common-annotation-2.1.0.jar;%APP_HOME%\lib\smallrye-config-common-3.3.0.jar;%APP_HOME%\lib\smallrye-common-classloader-2.1.0.jar;%APP_HOME%\lib\smallrye-common-expression-2.1.0.jar;%APP_HOME%\lib\smallrye-common-function-2.1.0.jar;%APP_HOME%\lib\smallrye-common-constraint-2.1.0.jar;%APP_HOME%\lib\smallrye-common-io-2.1.0.jar;%APP_HOME%\lib\smallrye-common-os-2.1.0.jar;%APP_HOME%\lib\mutiny-zero-flow-adapters-1.0.0.jar;%APP_HOME%\lib\smallrye-context-propagation-2.1.0.jar;%APP_HOME%\lib\smallrye-context-propagation-api-2.1.0.jar;%APP_HOME%\lib\smallrye-context-propagation-storage-2.1.0.jar;%APP_HOME%\lib\smallrye-fault-tolerance-vertx-6.2.4.jar;%APP_HOME%\lib\jakarta.xml.bind-api-4.0.0.jar;%APP_HOME%\lib\jakarta.activation-api-2.1.2.jar;%APP_HOME%\lib\fhir-model-4.11.1.jar;%APP_HOME%\lib\jakarta.enterprise.cdi-api-4.0.1.jar;%APP_HOME%\lib\jakarta.annotation-api-2.1.1.jar;%APP_HOME%\lib\jakarta.el-api-5.0.1.jar;%APP_HOME%\lib\jakarta.inject-api-2.0.1.jar;%APP_HOME%\lib\jakarta.interceptor-api-2.1.0.jar;%APP_HOME%\lib\parsson-1.1.2.jar;%APP_HOME%\lib\jakarta.json-api-2.1.2.jar;%APP_HOME%\lib\jakarta.transaction-api-2.0.1.jar;%APP_HOME%\lib\jakarta.validation-api-3.0.2.jar;%APP_HOME%\lib\jakarta.ws.rs-api-3.1.0.jar;%APP_HOME%\lib\antlr4-runtime-4.10.1.jar;%APP_HOME%\lib\commons-lang3-3.12.0.jar;%APP_HOME%\lib\httpcore-4.4.16.jar;%APP_HOME%\lib\checker-qual-3.34.0.jar;%APP_HOME%\lib\microprofile-config-api-3.0.3.jar;%APP_HOME%\lib\microprofile-context-propagation-api-1.3.jar;%APP_HOME%\lib\graal-sdk-22.3.2.jar;%APP_HOME%\lib\commons-logging-jboss-logging-1.0.0.Final.jar;%APP_HOME%\lib\jboss-logging-annotations-2.2.1.Final.jar;%APP_HOME%\lib\jboss-threads-3.5.0.Final.jar;%APP_HOME%\lib\jboss-logging-3.5.1.Final.jar;%APP_HOME%\lib\jboss-logmanager-embedded-1.1.1.jar;%APP_HOME%\lib\slf4j-jboss-logmanager-2.0.0.Final.jar;%APP_HOME%\lib\json-smart-2.4.10.jar;%APP_HOME%\lib\accessors-smart-2.4.9.jar;%APP_HOME%\lib\asm-9.5.jar;%APP_HOME%\lib\reactive-streams-1.0.4.jar;%APP_HOME%\lib\jcl-over-slf4j-1.7.21.jar;%APP_HOME%\lib\thymeleaf-3.0.9.RELEASE.jar;%APP_HOME%\lib\slf4j-api-2.0.6.jar;%APP_HOME%\lib\wildfly-common-1.5.4.Final-format-001.jar;%APP_HOME%\lib\classgraph-4.8.154.jar;%APP_HOME%\lib\ognl-3.1.12.jar;%APP_HOME%\lib\javassist-3.29.2-GA.jar;%APP_HOME%\lib\swagger-annotations-2.2.14.jar;%APP_HOME%\lib\joda-time-2.1.jar;%APP_HOME%\lib\xpp3-1.1.4c.0.jar;%APP_HOME%\lib\ucum-1.0.3.jar;%APP_HOME%\lib\commons-logging-1.2.jar;%APP_HOME%\lib\commons-collections-3.2.2.jar;%APP_HOME%\lib\icu4j-72.1.jar;%APP_HOME%\lib\Saxon-HE-9.8.0-15.jar;%APP_HOME%\lib\xpp3_xpath-1.1.4c.jar;%APP_HOME%\lib\jcip-annotations-1.0-1.jar;%APP_HOME%\lib\encoder-1.2.3.jar;%APP_HOME%\lib\fhir-core-4.11.1.jar;%APP_HOME%\lib\jakarta.json-1.0.0.jar;%APP_HOME%\lib\jakarta.enterprise.lang-model-4.0.1.jar;%APP_HOME%\lib\commons-net-3.9.0.jar;%APP_HOME%\lib\attoparser-2.0.4.RELEASE.jar;%APP_HOME%\lib\unbescape-1.1.5.RELEASE.jar;%APP_HOME%\lib\service-1.12.0.jar


@rem Execute RestAPI
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %REST_API_OPTS%  -classpath "%CLASSPATH%"  %*

:end
@rem End local scope for the variables with windows NT shell
if %ERRORLEVEL% equ 0 goto mainEnd

:fail
rem Set variable REST_API_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
set EXIT_CODE=%ERRORLEVEL%
if %EXIT_CODE% equ 0 set EXIT_CODE=1
if not ""=="%REST_API_EXIT_CONSOLE%" exit %EXIT_CODE%
exit /b %EXIT_CODE%

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
