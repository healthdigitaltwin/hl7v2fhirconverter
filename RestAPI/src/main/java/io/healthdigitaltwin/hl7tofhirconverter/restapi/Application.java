package io.healthdigitaltwin.hl7tofhirconverter.restapi;

import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

import io.github.linuxforhealth.hl7.HL7ToFHIRConverter;

import org.jboss.logging.Logger;

@OpenAPIDefinition(
    info = @Info(
        title = "HL7 to FHIR Converter API",
        version = "1.0",
        description = "API for converting HL7 messages to FHIR format"
    )
)

@Path("/convert")
public class Application {

    private static final Logger LOG = Logger.getLogger(Application.class);

    private HL7ToFHIRConverter hl7ToFHIRConverter;

    public Application() {
        this.hl7ToFHIRConverter = new HL7ToFHIRConverter();
    }

    // For testing purposes
    public Application(HL7ToFHIRConverter hl7ToFHIRConverter) {
        this.hl7ToFHIRConverter = hl7ToFHIRConverter;
    }

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response convert(String hl7message) {
        try {
            String output = hl7ToFHIRConverter.convert(hl7message);
            return Response.ok(output).build();
        } catch (Exception e) {
            LOG.error("Error converting HL7 message", e);
            return Response.status(Response.Status.BAD_REQUEST)
                .entity("Error converting HL7 message: " + e.getMessage())
                .build();
        }
    }
}
