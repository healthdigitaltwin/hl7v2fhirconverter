package io.healthdigitaltwin.hl7tofhirconverter.restapi;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import jakarta.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.nio.file.Files;
import java.nio.file.Paths;

import io.github.linuxforhealth.hl7.HL7ToFHIRConverter;

public class ApplicationTest {

    @Mock
    private HL7ToFHIRConverter mockConverter;

    private Application application;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        application = new Application(mockConverter);
    }

    @Test
    public void testConvert() throws Exception {
        // Read the test HL7 message from the resource file
        String hl7message = new String(Files.readAllBytes(Paths.get("src/test/resources/hl7message.hl7")));

        // Read the expected FHIR response from the resource file
        String expectedOutput = new String(Files.readAllBytes(Paths.get("src/test/resources/FHIRresponse.json")));

        // Mock the converter behavior
        when(mockConverter.convert(hl7message)).thenReturn(expectedOutput);

        // Call the method under test
        Response response = application.convert(hl7message);

        // Validate the response
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals(expectedOutput, response.getEntity());
    }
}
